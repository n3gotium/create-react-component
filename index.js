#! /usr/bin/env node

const fs = require('fs');

const path = require('path');

const args = process.argv.slice(2);

const help = (args.includes('-h')) || (args.includes('--help'));

if (help) {
  console.log([
    'usage: create-react-component <filename> [options]'
  ].join('\n'));
  process.exit();
}

const component = args.indexOf('--name') > -1 ? args[args.indexOf('--name') + 1] : null;

const directory = args.indexOf('--path') > -1 ? args[args.indexOf('--path') + 1] : null;

const templates = args.indexOf('--tpls') > -1 ? args[args.indexOf('--tpls') + 1] : null;

if (!component) {
  throw new Error('Component name must be provided by setting `--name` argumnet.');
}

createFiles(directory, component, templates);

function createDirectory(directory) {
  fs.mkdir(directory, (err, stdout) => {
    if (err) { throw err };
  });
}

function createFiles(directory, component) {
  const compDir = path.resolve(directory + component);

  createDirectory(compDir);

  const tplsDir = templates || path.join(__dirname, 'templates');

  fs.readdir(tplsDir, (err, files) => {
    if (err) { throw err };

    files.forEach(file => {
      fs.readFile(path.resolve(tplsDir, file), 'utf8', (err, data) => {
        if (err) { throw err };

        const fileName = substituteCompName(component, file.replace('.tpl', ''));

        const filePath = path.resolve(compDir, fileName);

        const fileData = substituteCompName(component, data);

        fs.writeFile(filePath, fileData, err => {
          if (err) { throw err };
        });
      });
    });
  });
}

function substituteCompName(name, string) {
  return string.replace(/__Comp__/g, name.replace(/^\w/, c => c.toUpperCase())).replace(/__comp__/g, name.replace(/^\w/, c => c.toLowerCase()));
}