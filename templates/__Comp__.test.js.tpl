import React from 'react';
import ReactDOM from 'react-dom';
import __Comp__ from './__Comp__';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<__Comp__ />, div);
  ReactDOM.unmountComponentAtNode(div);
});
