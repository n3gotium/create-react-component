import React from 'react';

import classNames from './__Comp__.module.css';

export default function __Comp__(props) {
  return (
    <div className={classNames.root}>
      { props.children }
    </div>
  );
};
